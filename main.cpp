#include <iostream>
#include <stack>
using namespace std;

template <typename T>
struct node {
    T data;
    struct node *l, *r;
    // l for left link, r for right link
    explicit node(T k) : data(k), l(nullptr), r(nullptr) {}
};

template <typename T>
class tree {
public:
    node<T> *root;
    size_t size;

    tree() : root(nullptr), size(0){}

    void insert(T value) {
        if (root == nullptr) {
            // If the tree is empty, create a new node and set it as the root.
            root = new node<T>(value);
            size++;
            return;
        }

        node<T>* tmp = root;
        while (tmp->l != nullptr || tmp->r != nullptr) {
            // Traverse the tree to find the correct position to insert the new value.
            // If the value already exists in the tree, return without inserting again.
            if (tmp->data == value) return;

            if (tmp->data > value) {
                // If the current node's value is greater than the new value, move to the left child.
                if (tmp->l == nullptr) {
                    // If the left child is null, insert the new node here and update the size.
                    tmp->l = new node<T>(value);
                    size++;
                } else {
                    // If the left child is not null, move to the left child and continue traversing.
                    tmp = tmp->l;
                }
            } else {
                // If the current node's value is less than the new value, move to the right child.
                if (tmp->r == nullptr) {
                    // If the right child is null, insert the new node here and update the size.
                    tmp->r = new node<T>(value);
                    size++;
                } else {
                    // If the right child is not null, move to the right child and continue traversing.
                    tmp = tmp->r;
                }
            }
        }

        // At this point, we have reached the correct position to insert the value.
        // Insert the new node as the left or right child of the current node, depending on the value.
        if (tmp->data > value) {
            tmp->l = new node<T>(value);
            size++;
        } else if (tmp->data < value) {
            tmp->r = new node<T>(value);
            size++;
        }
    }
    void remove(T key) {
        node<T>* tmp = root;
        // Parent node of the node to be removed
        node<T>* mroot = nullptr;
        // 0 for left child, 1 for right child
        int r = 0;

        // Search for the node with the given key value
        while (tmp != nullptr) {
            // Node found, exit the loop
            if (tmp->data == key) break;
            // Update the parent node
            mroot = tmp;
            if (tmp->data > key) {
                // If the current node's value is greater than the key, move to the left child
                tmp = tmp->l;
                r = 0;
            } else {
                // If the current node's value is less than the key, move to the right child
                tmp = tmp->r;
                r = 1;
            }
        }

        if (tmp == nullptr)
            return; // Key not found in the tree, nothing to remove

        // Case 1: Node has no children (leaf node)
        if (tmp->l == nullptr && tmp->r == nullptr) {
            if (mroot == nullptr)
                // If the node is the root, set root to nullptr
                root = nullptr;
            else if (r == 1)
                // If it is the right child of its parent, set parent's right to nullptr
                mroot->r = nullptr;
            else
                // If it is the left child of its parent, set parent's left to nullptr
                mroot->l = nullptr;

            // Delete the node
            delete tmp;
            size--;
        }
            // Case 2: Node has two children
        else if (tmp->l != nullptr && tmp->r != nullptr) {
            // Find the maximum value in the left subtree to replace the current node
            T maxi = max(tmp->l);
            // Replace the current node's value with the maximum value from the left subtree
            tmp->data = maxi;
            tmp = tmp->l;

            // Find the node with the value to be removed in the left subtree
            while (tmp->data != maxi)
                tmp = tmp->r;

            // Handle the case where the node to be removed has only one child in the left subtree
            if (tmp->l != nullptr && tmp->r == nullptr) {
                tmp->data = tmp->l->data;
                delete tmp->l;
                tmp->l = nullptr;
                size--;
            }
                // Handle the case where the node to be removed has only one child in the right subtree
            else if (tmp->r != nullptr && tmp->l == nullptr) {
                tmp->data = tmp->r->data;
                delete tmp->r;
                tmp->r = nullptr;
                size--;
            }
        }
            // Case 3: Node has one child (either left or right)
        else if (tmp->l != nullptr) {
            if (mroot == nullptr) {
                // If the node is the root, set root to its left child
                root = tmp->l;
            }
            else if (r == 1) {
                // If it is the right child of its parent, set parent's right to its left child
                mroot->r = tmp->l;
            }
            else {
                // If it is the left child of its parent, set parent's left to its left child
                mroot->l = tmp->l;
            }

            delete tmp; // Delete the node
            size--;
        }
        else if (tmp->r != nullptr) {
            // If the node is the root, set root to its right child
            if (mroot == nullptr) {
                root = tmp->r;
            }
            else if (r == 1) {
                // If it is the right child of its parent, set parent's right to its right child
                mroot->r = tmp->r;
            }
            else {
                // If it is the left child of its parent, set parent's left to its right child
                mroot->l = tmp->r;
            }
            // Delete the node
            delete tmp;
            size--;
        }

    }


    bool search(T value) {
        if(size == 0) return false;
        node<T>* tmp = root;
        while(tmp!= nullptr) {
            // if data is found return true
            if(tmp->data == value) return true;
            // go to left child if value is less than root
            if(tmp->data > value)tmp = tmp->l;
            // go to right child if value is greater than root
            else tmp = tmp->r;

        }

        return false;
    }
    void inorder() {
        _in(root);
    }
    void preorder() {  _pre(root);}
    void postorder() {_post(root);}
    T max(){
        auto tmp = root;

        while(tmp->r != nullptr) tmp = tmp->r;
        return tmp->data;
    }
    T max(node<T> *start){

        auto tmp = start;
        while(tmp->r != nullptr) tmp = tmp->r;
        return tmp->data;
    }
    T min(){
        auto tmp = root;
        while(tmp->l != nullptr) tmp = tmp->l;
        return tmp->data;
    }
    T min(node<T> *start){
        auto tmp = start;
        while(tmp->l != nullptr) tmp = tmp->l;
        return tmp->data;
    }
private:
    void _in(node<T> *start) {
        // Non-Recursive Inorder Traversal:
        // This function performs an iterative inorder traversal of the binary tree
        // starting from the given 'start' node and prints the elements in ascending order.
        // It uses a stack to keep track of the nodes to be visited.
        // The current node is initially set to 'start'.
        // The function traverses the left subtree of the current node first and pushes
        // each node encountered onto the stack until it reaches the leftmost node.
        // Then, it pops a node from the stack, prints its data, and moves to its right child.
        // The process continues until all nodes have been visited.
        stack<node<T>*> stk;
        node<T>* current = start;

        while (current != nullptr || !stk.empty()) {
            while (current != nullptr) {
                stk.push(current);
                current = current->l;
            }

            current = stk.top();
            stk.pop();
            cout << current->data << "\t";

            current = current->r;
        }
    }

    void _pre(node<T> *start) {
        // Non-Recursive Preorder Traversal:
        // This function performs an iterative preorder traversal of the binary tree
        // starting from the given 'start' node and prints the elements in preorder sequence.
        // It uses a stack to keep track of the nodes to be visited.
        // The current node is initially set to 'start'.
        // The function prints the data of the current node, then pushes its right child onto
        // the stack (if exists) and finally moves to its left child to explore the left subtree.
        // The process continues until all nodes have been visited.
        if (start == nullptr) return;

        stack<node<T>*> stk;
        stk.push(start);

        while (!stk.empty()) {
            node<T>* current = stk.top();
            stk.pop();
            cout << current->data << "\t";

            if (current->r != nullptr) {
                stk.push(current->r);
            }

            if (current->l != nullptr) {
                stk.push(current->l);
            }
        }
    }

    void _post(node<T> *start) {
        // Non-Recursive Postorder Traversal:
        // This function performs an iterative postorder traversal of the binary tree
        // starting from the given 'start' node and prints the elements in postorder sequence.
        // It uses a stack to keep track of the nodes to be visited.
        // The current node is initially set to 'start'.
        // The function traverses the left subtree of the current node first and pushes each
        // node encountered onto the stack until it reaches the leftmost node.
        // Then, it starts to backtrack from the leftmost node by popping nodes from the stack.
        // While backtracking, it prints the data of the current node only if either it has no
        // right child or its right child has already been visited (lastVisited).
        // If the right child has not been visited yet, it means the current node still has
        // unexplored right subtree, so it moves to the right child and explores its left subtree.
        // The process continues until all nodes have been visited.
        stack<node<T>*> stk;
        node<T>* current = start;
        node<T>* lastVisited = nullptr;

        while (current != nullptr || !stk.empty()) {
            while (current != nullptr) {
                stk.push(current);
                current = current->l;
            }

            current = stk.top();

            if (current->r == nullptr || current->r == lastVisited) {
                cout << current->data << "\t";
                stk.pop();
                lastVisited = current;
                current = nullptr;
            }
            else {
                current = current->r;
            }
        }
    }
};



int main() {
    tree<int> tf;
    tf.insert(20);
    tf.insert(21);
    tf.insert(24);
    tf.insert(19);
    tf.insert(15);
    tf.insert(145);

    cout<<endl<<"Inorder: "<<endl;
    tf.inorder();
    cout<<endl<<"preorder: "<<endl;
    tf.preorder();
    cout<<endl<<"postorder: "<<endl;
    tf.postorder();
    cout<<endl<<tf.search(100)<<endl;
    tf.remove(21);
    tf.inorder();
    cout<<endl;
    tf.remove(24);
    tf.inorder();

    return 0;
}