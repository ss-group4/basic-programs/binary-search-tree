#include <iostream>

template <typename T>
struct node {
    T data;
    node<T> *left, *right;
    explicit node(T k) : data(k), left(nullptr), right(nullptr) {}
};

template <typename T>
class tree {
public:
    node<T> *root;
    size_t size;

    tree() : root(nullptr), size(0) {}

    void insert(T value) {
        root = insert_recursive(root, value);
    }

    void remove(T key) {
        root = remove_recursive(root, key);
    }

    bool search(T value) {
        return search_recursive(root, value);
    }

    void inorder() {
        inorder_recursive(root);
    }

    void preorder() {
        preorder_recursive(root);
    }

    void postorder() {
        postorder_recursive(root);
    }

    T max() {
        return max_recursive(root);
    }

    T min() {

        return min_recursive(root);
    }

private:
    node<T> *insert_recursive(node<T> *root, T value) {
        if (root == nullptr) {
            size++;
            return new node<T>(value);
        }

        if (value < root->data) {
            root->left = insert_recursive(root->left, value);
        } else if (value > root->data) {
            root->right = insert_recursive(root->right, value);
        }

        return root;
    }

    node<T> *remove_recursive(node<T> *start, T key) {
        if (start == nullptr) {
            return start;
        }

        if (key < start->data) {
            start->left = remove_recursive(start->left, key);
        }
        else if (key > start->data) {
            start->right = remove_recursive(start->right, key);
        }
        else {
            if (start->left == nullptr) {
                node<T> *temp = start->right;
                delete start;
                size--;
                return temp;
            } else if (start->right == nullptr) {
                node<T> *temp = start->left;
                delete start;
                size--;
                return temp;
            }

            node<T> *minRight = findMin(start->right);
            start->data = minRight->data;
            start->right = remove_recursive(start->right, minRight->data);
        }

        return start;
    }

    bool search_recursive(node<T> *start, T value) {
        if (start == nullptr) {
            return false;
        }

        if (value == start->data) {
            return true;
        } else if (value < start->data) {
            return search_recursive(start->left, value);
        } else {
            return search_recursive(start->right, value);
        }
    }

    void inorder_recursive(node<T> *start) {
        if (start == nullptr) {
            return;
        }

        inorder_recursive(start->left);
        std::cout << start->data << "\t";
        inorder_recursive(start->right);
    }

    void preorder_recursive(node<T> *start) {
        if (start == nullptr) {
            return;
        }


        std::cout << start->data << "\t";
        preorder_recursive(start->left);
        preorder_recursive(start->right);
    }

    void postorder_recursive(node<T> *start) {
        if (start == nullptr)
            return;
        postorder_recursive(start->left);
        postorder_recursive(start->right);
        std::cout << start->data << "\t";
    }

    node<T> *findMin(node<T> *start) {
        while (start->left != nullptr) {
            start = start->left;
        }
        return start;
    }

    T max_recursive(node<T> *start) {
        if (start == nullptr) {
            throw std::runtime_error("Tree is empty");
        }

        while (start->right != nullptr) {
            start = start->right;
        }
        return start->data;
    }

    T min_recursive(node<T> *root) {
        if (root == nullptr) {
            throw std::runtime_error("Tree is empty");
        }

        while (root->left != nullptr) {
            root = root->left;
        }
        return root->data;
    }
};

int main() {
    tree<int> tf;
    tf.insert(20);
    tf.insert(21);
    tf.insert(24);
    tf.insert(19);
    tf.insert(15);
    tf.insert(145);

    std::cout << "Inorder: " << std::endl;
    tf.inorder();
    std::cout << std::endl << "Preorder: " << std::endl;
    tf.preorder();
    std::cout << std::endl << "Postorder: " << std::endl;
    tf.postorder();
    std::cout << std::endl;

    std::cout << tf.search(100) << std::endl;
    tf.remove(21);
    tf.inorder();
    std::cout << std::endl;
    tf.remove(24);
    tf.inorder();
    std::cout << std::endl;

    return 0;
}
